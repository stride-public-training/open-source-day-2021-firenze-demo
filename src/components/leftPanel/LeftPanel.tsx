import "./LeftPanel.css";

function LeftPanel() {
  return (
    <div className="left-panel">
      <div className="left-panel-header">
        <h1>Open Source Demo</h1>
        <h2>
          Stride <a href="https://stride-it.com"> Website </a> -
          <a href="https://stride-it.com"> Linkedin </a> -
          <a href="https://stride-it.com"> Instagram </a>
        </h2>
      </div>
    </div>
  );
}

export default LeftPanel;
